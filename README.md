# Practical Challenge #

A repo to demonstrate working with Renode to simulate a Nordic 52840 Dev Kit.

Development Environment Setup:
- SEGGER embedded studios - https://www.segger.com/downloads/embedded-studio/
- Nordic SDK - https://infocenter.nordicsemi.com/topic/sdk_nrf5_v17.0.2/nrf51_getting_started.html
- Renode - https://github.com/renode/renode

How to Run:

1) Start Renode
2) Enter into console: s @<repo path>\run_script
3) Watch the image run